#include "boundaryConditions.hh"
#include "poro.hh"
#include "poro_time.hh"
#include "backwardEuler.hh"
#include "prec.hh"

#include "geneo/neumann_boundary_condition.hh"
#include "geneo/partitionofunity.hh"
#include "geneo/subdomainbasis.hh"
#include "geneo/conbase_fork.hh"
#include "geneo/localoperator_ovlp_region.hh"
#include "geneo/geneobasis.hh"
#include "geneo/two_level_schwarz.hh"
#include "geneo/subdomainprojectedcoarsespace.hh"

template <typename MODEL, typename GRID, typename HELPER>
void fem_driver_geneo(MODEL& model, const GRID& grid, HELPER& helper,bool verb = true){

    double time = 0.0; // Initialise time

    double dt = model.get_dt();

    // Get leafGridView from grid
    typedef typename GRID::LeafGridView GV;
    GV gv = grid.leafGridView();

    const int dim = GV::Grid::dimension;
    typedef typename GV::Grid::ctype Coord;

   // Construct FEM Spaces
    typedef Dune::PDELab::QkLocalFiniteElementMap<GV,typename GV::Grid::ctype,double,2> FEM_P2;
        FEM_P2 P2(gv);
    typedef Dune::PDELab::QkLocalFiniteElementMap<GV,typename GV::Grid::ctype,double,1> FEM_P1;
        FEM_P1 P1(gv);

  // Construct grid function spaces for each degree of freedom

  // Construct grid function spaces for each degree of freedom
  typedef Dune::PDELab::OverlappingConformingDirichletConstraints CON;
    CON con;

    typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::none,1> VBE;

    typedef Dune::PDELab::GridFunctionSpace<GV, FEM_P1, CON, VBE> P1_GFS;
    typedef Dune::PDELab::GridFunctionSpace<GV, FEM_P2, CON, VBE> P2_GFS;

    P2_GFS dispU(gv,P2); dispU.name("U");
    P2_GFS dispV(gv,P2); dispV.name("V");
    P2_GFS dispW(gv,P2); dispW.name("W");
    
    P1_GFS pressure(gv,P1); pressure.name("PRESSURE");

  typedef Dune::PDELab::CompositeGridFunctionSpace <VBE,Dune::PDELab::LexicographicOrderingTag, P2_GFS, P2_GFS, P2_GFS,P1_GFS> GFS; 
    GFS gfs(dispU, dispV, dispW,pressure);

 // Assemble Constraints
 typedef typename GFS::template ConstraintsContainer<double>::Type C;
  auto cc = C();
  auto cc_exterior = C();
  auto cc_bnd_neu_int_dir = C();

  // assemble constraints
  cc.clear();
  cc_exterior.clear();
  cc_bnd_neu_int_dir.clear();

  typedef Scalar_BC<GV,double,MODEL> BC;
    BC U_cc(gv,model), V_cc(gv,model), W_cc(gv,model),p_cc(gv,model);
        U_cc.setDof(1); 
        V_cc.setDof(2); 
        W_cc.setDof(3); 
        p_cc.setDof(4); 

  typedef Dune::PDELab::CompositeConstraintsParameters<BC,BC,BC,BC> Constraints;
    Constraints constraints(U_cc,V_cc,W_cc,p_cc);

  Dune::PDELab::constraints(constraints,gfs,cc);
  
  Dune::PDELab::constraints_exterior(constraints,gfs,cc_exterior);

  Dune::PDELab::PureNeumannBoundaryCondition pnbc;
  
  Dune::PDELab::constraints(pnbc,gfs,cc_bnd_neu_int_dir);


  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
    MBE mbe(402); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().//


  typedef PoroElasticity_K<MODEL,81,8> K_LOP;
    K_LOP k_lop(model,dt,5,false);
    K_LOP k_block_diag_lop(model,dt,5,true); // Operator for preconditioner

  typedef Dune::PDELab::GridOperator<GFS,GFS,K_LOP,MBE,double,double,double,C,C> K_GO;
   K_GO k_go(gfs,cc,gfs,cc,k_lop,mbe);
  
  /*typedef PoroElasticity_M<MODEL,81,8> M_LOP;
    M_LOP m_lop(model,dt);

  typedef Dune::PDELab::GridOperator<GFS,GFS,M_LOP,MBE,double,double,double,C,C> M_GO;
   M_GO m_go(gfs,cg,gfs,cg,m_lop,mbe);*/

  // << Make FE function with initial values >>
  typedef typename K_GO::Traits::Domain X;
    X x(gfs,0.0);

  typedef typename K_GO::Jacobian J;

  BC u(gv,model), v(gv,model), w(gv,model), p(gv,model);
    u.setDof(1);  v.setDof(2);  w.setDof(3), p.setDof(4);

  typedef Dune::PDELab::CompositeGridFunction<BC,BC,BC,BC> InitialSolution;
    InitialSolution initial_solution(u,v,w,p);

  Dune::PDELab::interpolate(initial_solution,gfs,x);

  // Setup Partition of Unity Operator

  typedef Dune::PDELab::LocalFunctionSpace<GFS, Dune::PDELab::AnySpaceTag> LFS;
    LFS lfs(gfs);

  std::shared_ptr<X> part_unity;
  
  part_unity = standardPartitionOfUnity<3,X>(gfs, lfs, cc_bnd_neu_int_dir);

  if (configuration.get<bool>("PartitionVTK",false)) {
    Dune::SubsamplingVTKWriter<GV> vtkwriter(gfs.gridView(),0);
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,*part_unity);
    vtkwriter.write("partition",Dune::VTK::appendedraw);
  }

  J AF(k_go);

  AF = 0.0; k_go.jacobian(x,AF);

  int nev = configuration.get<int>("eigenvectors",1);
  int nev_arpack = configuration.get<int>("eigenvectors_compute",-1);

  std::shared_ptr<SubdomainBasis<X> > subdomain_basis;

  //  Matrix with pure neumann boundary conditions
    K_GO k_block_go(gfs,cc_exterior,gfs,cc_exterior,k_block_diag_lop,mbe);
    
    J AF_neu(k_block_go);
    
    AF_neu = 0.0;

    k_block_go.jacobian(x,AF_neu);

  // Neuman on only the overlapping region
  typedef Dune::PDELab::LocalOperatorOvlpRegion<K_LOP, GFS> LOP_OVLP;
  
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP_OVLP,MBE,double,double,double,C,C> GO_OVLP;
    LOP_OVLP lop_ovlp(k_block_diag_lop, gfs);

  
  auto go_overlap = GO_OVLP(gfs,cc_exterior,gfs,cc_exterior,lop_ovlp,mbe);
  J AF_ovlp(go_overlap);
  AF_ovlp = 0.0;
  go_overlap.jacobian(x,AF_ovlp); // assemble fine grid matrix


  subdomain_basis = std::make_shared<GenEOBasis<GFS,J,X,X,1> >(gfs, AF_neu, AF_ovlp, 1.0, *part_unity, nev, nev_arpack, configuration.get<double>("sigma",0.001));

  if (configuration.get<bool>("EigenvectorVTK",false)) {
    for (int i = 0; i < subdomain_basis->local_basis.size(); i++) {
      Dune::SubsamplingVTKWriter<GV> vtkwriter(gfs.gridView(),0);
      X eigenvector(gfs, *subdomain_basis->local_basis[i]);
      Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,eigenvector);
      vtkwriter.write("eigenvector"+std::to_string(i),Dune::VTK::appendedraw);
    }
  }

  auto partunityspace = std::make_shared<SubdomainProjectedCoarseSpace<GFS,J,X,X,1> >(gfs, AF_neu, subdomain_basis, 1);
  typedef TwoLevelOverlappingAdditiveSchwarz<GFS,J,X,X> PREC;
  auto prec = std::make_shared<PREC>(gfs, AF, partunityspace);

  //Set up solution vector and some necessary operators
   
  typedef Dune::PDELab::OverlappingOperator<C,J,X,X> POP;
  
  auto popf = std::make_shared<POP>(cc,AF);

  typedef Dune::PDELab::istl::ParallelHelper<GFS> PIH;
    PIH pihf(gfs);
    
  typedef Dune::PDELab::OverlappingScalarProduct<GFS,X> OSP;
    OSP ospf(gfs,pihf);


  Dune::InverseOperatorResult result;
  
  auto solver = std::make_shared<Dune::RestartedGMResSolver<X> >(*popf,ospf,*prec,1e-6,25,5000,1,true);
    

  /*
  //Set up Grid operator
  typedef typename K_GO::Jacobian J;

  //  Matrix with "correct" boundary conditions
    K_GO go(gfs,cg,gfs,cg,k_lop,mbe);
    J AF(go);
    AF = 0.0;
    go.jacobian(x,AF);
 
  //  Matrix with pure neumann boundary conditions
    K_GO go_neu(gfs,cg_ext,gfs,cg_ext,k_lop,mbe);
    J AF_neu(go_neu);
        AF_neu = 0.0;
        go_neu.jacobian(x,AF_neu);


  // =========================================================================================
  //           Construct Block-Preconditioner, Blocks are solved with GENEO preconditioner
  // =========================================================================================
  
  typedef Prec_K<MODEL,24,8> Prec_LOP;
    Prec_LOP prec_lop(model,dt);

  // Construct with Dirichlet Condition on nodes with boudnary data - Neumann on Processor Boundary

  typedef Dune::PDELab::GridOperator<GFS,GFS,Prec_LOP,MBE,double,double,double,C,C> Prec_GO;
   Prec_GO go_prec(gfs,cg,gfs,cg,prec_lop,mbe);

  typedef typename Prec_GO::Jacobian J_Prec;

  J_Prec PF(go_prec); PF = 0.0; go_prec.jacobian(x,PF); // Full Preconditioner Matrix with "Correct" boundary conditions

  Prec_GO go_prec_neu(gfs,cg_ext,gfs,cg_ext,prec_lop,mbe);

  J_Prec PF_neu(go_prec_neu); PF_neu = 0.0; go_prec_neu.jacobian(x,PF_neu); // Matrix with pure neumann boundary conditions

  typedef Dune::PDELab::LocalOperatorOvlpRegion<Prec_LOP, GFS> LOP_ovlp;
      LOP_ovlp lop_ovlp(prec_lop,gfs);
    
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP_ovlp,MBE,double,double,double,C,C> GO_ovlp;
      GO_ovlp go_ovlp(gfs,cg_ext,gfs,cg_ext,lop_ovlp,mbe); // Note use of Neumann boundaries for right hand side of eigenvalue problem

  typedef typename GO_ovlp::Jacobian J_overlap;
      J_overlap PF_ovlp(go_ovlp);
    
    PF_ovlp = 0.0;  go_ovlp.jacobian(x,PF_ovlp);

   // Extract information about local basis functions spaces

    typedef Dune::PDELab::LocalFunctionSpace<GFS, Dune::PDELab::AnySpaceTag> LFSU;
  
    typedef typename LFSU::template Child<0>::Type LFS;
      LFSU lfsu(gfs);
      LFS lfs = lfsu.template child<0>(); // Note this is where the bug is when you implement a mixed finite element space

   // ==== Finally setup the Block GENEO preconditioner

    typedef TwoLevelOverlappingSchwarzDistributedCoarseGrid<GFS,LFS,C,J,X,X,4> PREC;
        PREC prec(gfs, lfs, cg, cg_ext, PF, PF_neu, PF_ovlp,1e-5); // threshold not set

    //Set up solution vector and some necessary operators
   
    typedef Dune::PDELab::OverlappingOperator<C,J,X,X> POP;
      POP popf(cg,AF);
    typedef Dune::PDELab::istl::ParallelHelper<GFS> PIH;
      PIH pihf(gfs);
    typedef Dune::PDELab::OverlappingScalarProduct<GFS,X> OSP;
      OSP ospf(gfs,pihf);

    typedef Dune::RestartedGMResSolver<X> LS;
      LS ls(popf,ospf,prec,configuration.get<double>("linear_solver_tolerance",1e-8),configuration.get<double>("gmres_restart",100),configuration.get<int>("linear_solver_max_iterations",5000),configuration.get<bool>("solver_verbose",true));



    // Solve Problem

    X xnew(gfs,0.0);

    Dune::PDELab::interpolate(initial_solution,gfs,xnew);

    X z(gfs,0.0); // Setup up corrector vector
    X d(gfs,0.0); // Container for the defect

    go.residual(xnew,d);

    X M_x0(gfs,0.0); m_go.residual(x,M_x0);

    d -= M_x0;

    Dune::InverseOperatorResult result;

    ls.apply(z,d,result);

    xnew -= z; // Update the solution

    int tstep = 0;

    stringstream sstm;
    sstm << "Solution_" << tstep;
    string file_output_name = sstm.str();

    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,0);
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,xnew);
    vtkwriter.write(file_output_name,Dune::VTK::appendedraw);
    
    */


} // End Cosserat_driver
