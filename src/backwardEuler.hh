template<class K_GO, class M_GO, typename LS, typename V>
    class BackwardEuler
    {
      typedef typename Dune::template FieldTraits<typename V::ElementType >::real_type Real;
      typedef typename K_GO::Traits::Jacobian M;
      typedef typename K_GO::Traits::TrialGridFunctionSpace TrialGridFunctionSpace;
      using W = Dune::PDELab::Backend::Vector<TrialGridFunctionSpace,typename V::ElementType>;
      

    public:
     // typedef BackwardEulerStepResult<double> Result;

      BackwardEuler(const K_GO& _kgo, const M_GO& _mgo, LS& _ls, double _reduction, double _min_defect = 1e-99, bool _verbose = true)
        : k_go(_kgo)
        , m_go(_mgo)
        , linearSolver(_ls)
        , reduction(_reduction)
        , min_defect(_min_defect)
        , verb(_verbose)
      {}

      inline void apply(V& x0,V& x1){

      	// Applies a single step of Backward Euler Solver

        if(!Jac){
  	     Jac = std::make_shared<M>(k_go);
  	    }


        V M_x0(k_go.testGridFunctionSpace(),0.0); m_go.residual(x0,M_x0);

    	  auto defect = 1.0;

        int k = 0;

        while (defect > 1.0e-10){ // Newton Iterations

           std::cout << "It. k = " << k << " Defect = " << defect << std::endl;

          // Initialise Residual for this loop

          V r(k_go.testGridFunctionSpace(),0.0);

          k_go.residual(x1,r);

          r -= M_x0;


          V z(k_go.trialGridFunctionSpace(),0.0); // Initialise Corrector

          // Compute Jacobian at x
          (*Jac) = Real(0.0); k_go.jacobian(x1,*Jac);

          linearSolver.apply(*Jac,z,r,reduction); // solver makes right hand side consistent

          x1 -= z; // Update the solution
 

          V post_r(k_go.testGridFunctionSpace(),0.0);
          k_go.residual(x1,post_r);
          post_r -= M_x0;
          defect = linearSolver.norm(post_r);

          std::cout << "Residal after solve = " << defect << std::endl;

          k += 1; // Increment iteration counter

        }



        

        /*std::cout << "Defect = " << defect << std::endl;

        V z(k_go.trialGridFunctionSpace(),0.0);

    	  auto red = std::max(reduction,min_defect/defect);

    	  linearSolver.apply(*Jac,z,r,red); // solver makes right hand side consistent

        std::cout << "Norm of corrector = " << linearSolver.norm(z) << std::endl;

    	  x -= z; // Update solution with corrector

        std::cout << "Norm of solution = " << linearSolver.norm(x) << std::endl;
        */

}

private:

	const K_GO& k_go;
	const M_GO& m_go;
  double reduction;
  double min_defect;
  bool verb;
    LS& linearSolver;
	shared_ptr<M> Jac;


};

