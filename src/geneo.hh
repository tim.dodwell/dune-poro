#ifndef DUNE_GENEO_HH
#define DUNE_GENEO_HH

// add your classes here
#include "geneo/two_level_schwarz.hh"
#include "geneo/gridtraits.hh"
#include "geneo/cg_fork.hh"
#include "geneo/cg_fork_inf_norm.hh"
#include "geneo/localoperator_ovlp_region.hh"
#include "geneo/conbase_fork.hh"

#endif // DUNE_GENEO_HH
