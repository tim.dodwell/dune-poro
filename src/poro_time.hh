#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>

//#include "defaultimp.hh"
//#include "pattern.hh"
//#include "flags.hh"
//#include "idefault.hh"

using namespace std;

        
template<typename PARAM, int udof, int pdof>
class PoroElasticity_M:
        public Dune::PDELab::NumericalJacobianApplyVolume<PoroElasticity_M<PARAM,udof,pdof>>,
        public Dune::PDELab::NumericalJacobianVolume<PoroElasticity_M<PARAM,udof,pdof>>,
        public Dune::PDELab::FullVolumePattern,
        public Dune::PDELab::LocalOperatorDefaultFlags
{
public:
// pattern assembly flags
    enum { doPatternVolume = true };
            
// residual assembly flags
    enum { doAlphaVolume = true };
    
PoroElasticity_M(const PARAM& param_, double dt_, unsigned int intorder_=6):param(param_), intorder(intorder_), dt(dt_){}
    
    
// Volume Integral Depending on Test and Ansatz Functions
template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
{
    // Galerkin Finite Elements - assumes lfsu == lfsv
    const int dim = 3;

    // Unwrap shape local function spaces
    typedef typename LFSU::template Child<0>::Type LFSU_U;
    typedef typename LFSU::template Child<1>::Type LFSU_V;
    typedef typename LFSU::template Child<2>::Type LFSU_W;
    typedef typename LFSU::template Child<3>::Type LFSU_P;
              
    const LFSU_U& lfsu_u = lfsu.template child<0>();
    const LFSU_V& lfsu_v = lfsu.template child<1>();
    const LFSU_W& lfsu_w = lfsu.template child<2>();
    const LFSU_P& lfsu_p = lfsu.template child<3>();

    const unsigned int usize = lfsu_u.size();

    const unsigned int psize = lfsu_p.size();


    // domain and range field type
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainType D;
    typedef typename R::value_type RF;
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LFSU_U::Traits::SizeType size_type;
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeType;
                
    // select quadrature rule
    auto geo = eg.geometry();
    
    const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(geo.type(),intorder);
        
    // Unwrap solution at node, into vector d
    Dune::FieldVector<double,udof> d(0.0);
    
    for (size_type i=0; i < usize; ++i){
        d[i] = x(lfsu_u,i); // U1
        d[i + usize] = x(lfsu_v,i); // U2
        d[i + 2 * usize] = x(lfsu_w,i); // Theta3
    }
    
    // Loop over quadrature points
    for (const auto& ip : rule)
    {
        // Evaluate shape functions at Integration Point
        std::vector<RangeType> phi(usize);    
        lfsu_u.finiteElement().localBasis().evaluateFunction(ip.position(),phi);
        
        // Evaluate gradients of shape function at integration point
        std::vector<JacobianType> js(usize);
        lfsu_u.finiteElement().localBasis().evaluateJacobian(ip.position(),js);
        
        // transform gradient to real element
        const typename EG::Geometry::JacobianInverseTransposed jac = geo.jacobianInverseTransposed(ip.position());
        
        std::vector<Dune::FieldVector<RF,dim> > gradphi(usize);
            for (size_t i=0; i<usize; i++){
                gradphi[i] = 0.0;
                jac.umv(js[i][0],gradphi[i]);
            }     

        // Compute B - such that Strain = B * d - Checked
        Dune::FieldMatrix<double,6,udof> B(0.0);
        for (size_type i = 0; i < usize; i++){
            B[0][i] = gradphi[i][0]; // E11
            B[1][i + usize] = gradphi[i][1]; // E22
            B[2][i + 2 * usize] = gradphi[i][2]; // E33
            B[3][i + usize] = gradphi[i][2]; B[3][i + 2 * usize] = gradphi[i][1]; // E23  = U2,3 + U3,2
            B[4][i] = gradphi[i][2]; B[4][i + 2 * usize] = gradphi[i][0]; // E13  = U1,3 + U3,1
            B[5][i] = gradphi[i][1]; B[5][i + usize] = gradphi[i][0]; // E12  = U1,2 + U2,1           
        }

        
        // Compute Darcy Term
    
        // -----------------------------------------------------------------
        //                          Coupling Term
        // -----------------------------------------------------------------
        
        // evaluate basis functions on reference element
        std::vector<RangeType> psi(lfsu_p.size());
        lfsu_p.finiteElement().localBasis().evaluateFunction(ip.position(),psi);


        Dune::FieldVector<double,6> delta(0.0);
        delta[0] = 1.0; delta[1] = 1.0; delta[2] = 1.0;
        
        Dune::FieldMatrix<double,udof,pdof> L(0.0);
        Dune::FieldVector<double,udof> tmp(0.0);
        
        B.mtv(delta,tmp);
        
        for (size_type i = 0; i < udof; i++){
            for (size_type j = 0; j < lfsu_p.size(); j++){
                L[i][j] = tmp[i] * psi[j];
            }
        }
        
 
        Dune::FieldVector<double,pdof> p_cpl_res(0.0);

        L.mtv(d,p_cpl_res);

        // geometric weight
        RF factor = geo.integrationElement(ip.position()) * ip.weight();
    
        
        for (size_type i=0; i<lfsu_p.size(); i++){
          r.accumulate(lfsu_p,i,-p_cpl_res[i]*factor);
        }
        
    } // end for each quadrature point
                
} // end alpha_volume
    
        private:
    
            const PARAM& param;
            int intorder;
            double dt;
};
        
