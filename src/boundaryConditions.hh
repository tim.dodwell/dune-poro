/** 
 \brief A function that defines Dirichlet Boundary conditions AND the extension to the interior of the domain
 
*/

#ifndef BoundaryConditions_h
#define BoundaryConditions_h

// Define Scalar Dirichlet Boundary Conditions
template<typename GV, typename RF, class MODEL>
class Scalar_BC :
public Dune::PDELab::AnalyticGridFunctionBase<
Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
Scalar_BC<GV,RF,MODEL> >,
public Dune::PDELab::InstationaryFunctionDefaults
{

private:
    int dof;
    MODEL & myModel;

public:

    typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
    typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, Scalar_BC<GV,RF,MODEL>> BaseT;

    typedef typename Traits::DomainType DomainType;
    typedef typename Traits::RangeType RangeType;

    // Constructor
    Scalar_BC(const GV & gv, MODEL& myModel_) :
        BaseT(gv), myModel(myModel_){ }

    template<typename I>
    bool isDirichlet(const I & ig,
                     const typename Dune::FieldVector<typename I::ctype, I::dimension-1> & x
                     ) const
    {
        const DomainType xg = ig.geometry().global(x);

        bool Dirich = false;
        if(myModel.isDirichlet(xg,dof)){
            Dirich = true;
        }
       
        return Dirich; // return bool
    }

    inline void evaluateGlobal(const DomainType & x, RangeType & u) const
    {

        if(myModel.isDirichlet(x,dof)){
            u = myModel.evaluateDirichlet(x,dof);
        }
        else{ u = 0.0; }

    } // end inline function evaluateGlobal

    void setDof(int degree_of_freedom){
        dof = degree_of_freedom;
    }

};




#endif /* BoundaryConditions_h */