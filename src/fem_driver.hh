#include "boundaryConditions.hh"
#include "poro.hh"
#include "poro_time.hh"
#include "backwardEuler.hh"
#include "mySolvers.hh"

template <typename MODEL, typename GRID>
void fem_driver(MODEL& model, const GRID& grid, bool verb = true){

    double time = 0.0; // Initialise time

    double dt = model.get_dt();

    // Get leafGridView from grid
    typedef typename GRID::LeafGridView GV;
    GV gv = grid.leafGridView();

  	const int dim = GV::Grid::dimension;
    typedef typename GV::Grid::ctype Coord;

	 // Construct FEM Spaces
    typedef Dune::PDELab::QkLocalFiniteElementMap<GV,typename GV::Grid::ctype,double,2> FEM_P2;
        FEM_P2 P2(gv);
	  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,typename GV::Grid::ctype,double,1> FEM_P1;
		    FEM_P1 P1(gv);

	// Construct grid function spaces for each degree of freedom

  // Construct grid function spaces for each degree of freedom
  typedef Dune::PDELab::OverlappingConformingDirichletConstraints CON;
    CON con;

    typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::none,1> VBE;

    typedef Dune::PDELab::GridFunctionSpace<GV, FEM_P1, CON, VBE> P1_GFS;
    typedef Dune::PDELab::GridFunctionSpace<GV, FEM_P2, CON, VBE> P2_GFS;

		P2_GFS dispU(gv,P2); dispU.name("U");
		P2_GFS dispV(gv,P2); dispV.name("V");
    P2_GFS dispW(gv,P2); dispW.name("W");
		
    P1_GFS pressure(gv,P1); pressure.name("PRESSURE");

	typedef Dune::PDELab::CompositeGridFunctionSpace <VBE,Dune::PDELab::LexicographicOrderingTag, P2_GFS, P2_GFS, P2_GFS,P1_GFS> GFS;
		GFS gfs(dispU, dispV, dispW,pressure);


 // Make constraints map and initialize it from a function
	typedef typename GFS::template ConstraintsContainer<double>::Type C;
		C cg;
  cg.clear();


	typedef Scalar_BC<GV,double,MODEL> BC;
		BC U_cc(gv,model), V_cc(gv,model), W_cc(gv,model),p_cc(gv,model);
        U_cc.setDof(1); 
        V_cc.setDof(2); 
        W_cc.setDof(3); 
        p_cc.setDof(4); 

	typedef Dune::PDELab::CompositeConstraintsParameters<BC,BC,BC,BC> Constraints;
    Constraints constraints(U_cc,V_cc,W_cc,p_cc);

	Dune::PDELab::constraints(constraints,gfs,cg);

  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
    MBE mbe(402); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().//


  typedef PoroElasticity_K<MODEL,81,8> K_LOP;
    K_LOP k_lop(model,dt);

  typedef Dune::PDELab::GridOperator<GFS,GFS,K_LOP,MBE,double,double,double,C,C> K_GO;
   K_GO k_go(gfs,cg,gfs,cg,k_lop,mbe);


  typedef PoroElasticity_M<MODEL,81,8> M_LOP;
    M_LOP m_lop(model,dt);

  typedef Dune::PDELab::GridOperator<GFS,GFS,M_LOP,MBE,double,double,double,C,C> M_GO;
   M_GO m_go(gfs,cg,gfs,cg,m_lop,mbe);


   // How well did we estimate the number of entries per matrix row?
  // => print Jacobian pattern statistics
  //typename K_GO::Traits::Jacobian jac(k_go);
  //std::cout << jac.patternStatistics() << std::endl;


  // << Make FE function with initial values >>
  typedef typename K_GO::Traits::Domain X;
    X x(gfs,0.0);

  BC u(gv,model), v(gv,model), w(gv,model), p(gv,model);
    u.setDof(1);  v.setDof(2);  w.setDof(3), p.setDof(4);

  typedef Dune::PDELab::CompositeGridFunction<BC,BC,BC,BC> InitialSolution;
    InitialSolution initial_solution(u,v,w,p);

  Dune::PDELab::interpolate(initial_solution,gfs,x);

  
    /*Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,0);
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,x);
    vtkwriter.write("initialSolution",Dune::VTK::appendedraw);*/
  
    /*typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack LS;
      LS ls(5000,false);*/

     // typedef Dune::PDELab::ISTLBackend_SEQ_SSOR LS;
       //   LS ls(5000,10);

     /* typedef Dune::PDELab::ISTLBackend_SEQ_LOOP_Jac LS;
        LS ls(5000,true);*/

  /*typedef Dune::PDELab::ISTLBackend_OVLP_CG_SSORk<GFS,C> LS;
    LS ls(gfs,cg,5000,5,1);
*/

    typedef OVLP_BCGS_UMFPACK<GFS,C> LS;
      LS ls(gfs,cg,500,1);

    //typedef Dune::PDELab::ISTLBackend_OVLP_BCGS_SuperLU<GFS, C> LS;
   // typedef Dune::PDELab::ISTLBackend_OVLP_CG_UMFPack<GFS, C> LS;  //CG with UMFPack
     // LS ls(gfs,cg,500,1);

    // Solve Problem

    X xnew(gfs,0.0);

    Dune::PDELab::interpolate(initial_solution,gfs,xnew);

    typedef Dune::PDELab::StationaryLinearProblemSolver<K_GO,LS,X> SLP;
      SLP slp(k_go,ls,xnew,1e-10);
    slp.apply();

    int tstep = 0;

    stringstream sstm;
    sstm << "Solution_" << tstep;
    string file_output_name = sstm.str();

    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,0);
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,xnew);
    vtkwriter.write(file_output_name,Dune::VTK::appendedraw);
    



    /*typedef Dune::PDELab::StationaryLinearProblemSolver<K_GO,LS,X> SLP;
      SLP slp(k_go,ls,xnew,1e-10);
    slp.apply();*/


 /* typedef BackwardEuler<K_GO,M_GO,LS,X> BE;
      BE bE(k_go,m_go,ls,model.getReduction());



    // << Time integration loop >>
      X xnew(gfs,0.0);
    Dune::PDELab::interpolate(initial_solution,gfs,xnew);

    int tstep = 0;

   while (time < 10.0 ){

      tstep += 1; // Increment timestep

      time += dt; // Increment time

      std::cout << "This is tstep = " << tstep << std::endl;

      bE.apply(x,xnew); // Apply backward Euler Step

      X res(gfs,0.0);

      m_go.residual(x,res);

      std::cout << "Residual = " << ls.norm(res) << std::endl;

      std::cout << "=================" << std::endl;

      if (verb){

        stringstream sstm;
        sstm << "Solution_" << tstep;
        string file_output_name = sstm.str();

        Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,0);
        Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,xnew);
        vtkwriter.write(file_output_name,Dune::VTK::appendedraw);
      }

      x = xnew;


   }*/



} // End Cosserat_driver
