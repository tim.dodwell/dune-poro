// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include<math.h>
#include<iostream>
#include<vector>
#include<map>
#include<string>
#include<sstream>
#include <mpi.h>

#include <config.h>

#include <dune/common/parametertree.hh>
Dune::ParameterTree configuration;

#include <dune/common/bitsetvector.hh>

#include <dune/grid/yaspgrid.hh> // Checked Inclusion
#include <dune/grid/common/gridview.hh>
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/common/gridinfo.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/istl/io.hh>
#include <dune/istl/matrixmarket.hh>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parallel/collectivecommunication.hh>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/yaspgrid/partitioning.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/io.hh>
#include <dune/istl/superlu.hh>




#include <dune/pdelab/newton/newton.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/finiteelementmap/pkfem.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/localfunctions/lagrange/qk.hh>
#include <dune/pdelab/finiteelementmap/rannacherturekfem.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/vectorgridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/instationary/onestep.hh>
#include <dune/pdelab/common/instationaryfilenamehelper.hh>
#include <dune/pdelab/instationary/implicitonestep.hh>
#include <dune/pdelab/instationary/explicitonestep.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/backend/istl/bcrsmatrixbackend.hh>
#include <dune/pdelab/stationary/linearproblem.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/gridoperator/onestep.hh>
#include <dune/pdelab/instationary/onestepparameter.hh>
#include <dune/grid/geometrygrid/grid.hh>
#include <dune/common/parametertree.hh>

#include "geometrytransform.hh"
#include "model.hh"
//#include "geneo.hh"
//#include "fem_driver.hh"
#include "fem_driver_geneo.hh"
//#include "fem_driver_block.hh"



// Comment

int main(int argc, char** argv)
{
  try{

   // Maybe initialize MPI
   Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

   Dune::MPIHelper::MPICommunicator mycomm = helper.getCommunicator();
  
   //Read ini file
   Dune::ParameterTreeParser parser;
   parser.readINITree(argv[1],configuration);

   const int dim = 3;

   std::string gridName = configuration.get<std::string>("grid_Location","grids/mySphere.msh");
   bool verb = configuration.get<bool>("verbosity",true);

   MODEL<dim> myModel(configuration);

   // Vectors for boundary and material conditions

   typedef Dune::YaspGrid<dim> YGRID;
   
   // Set up Yasp Grid

   typedef typename Dune::YaspGrid<dim> YGRID;
     YGRID yaspgrid(myModel.getL(),myModel.getN(),myModel.getPeriodic(),myModel.getOverlap());

   GridTransformation<MODEL<dim>, dim> mytransformation(myModel);
    typedef typename Dune::GeometryGrid<YGRID,GridTransformation<MODEL<dim>,dim>> GRID;
      GRID grid(yaspgrid,mytransformation);


  // Run finite element model
  std::string solverName = configuration.get<std::string>("solver","geneo_norm");   


  fem_driver_geneo<MODEL<dim>,GRID>(myModel,grid,verb); 

  //if(solverName == "geneo"){
   // fem_driver_geneo<MODEL<dim>,GRID,Dune::MPIHelper>(myModel,grid,helper,verb);
  //}
  //else{
  //  fem_driver_block<MODEL<dim>,GRID,Dune::MPIHelper>(myModel,grid,helper,verb);
  //}

    return 0;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}



