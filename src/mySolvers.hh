
#include "geneo/arpackpp_fork.hh"
#include "geneo/multicommdatahandle.hh"


#if HAVE_SUITESPARSE || DOXYGEN
    // exact subdomain solves with UMFPack as preconditioner
    template<class GFS, class M, class X, class Y ,class C>
    class OneLevel_OVLP_Schwarz : public Dune::Preconditioner<X,Y>
    {

    public:

      typedef Dune::PDELab::Backend::Native<M> ISTLM;
  	  typedef Dune::PDELab::Backend::Native<X> ISTLX;
      typedef typename GFS::Traits::GridView GV;

  	  typedef Dune::BlockVector<Dune::FieldVector<double,1> > COARSE_V;
      typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > COARSE_M;

      //! Type used to store owner rank values of all DOFs.
      typedef int RankIndex;
  	  using RankVector = Dune::PDELab::Backend::Vector<GFS,RankIndex>;

	  //typedef typename GFS::Traits::ConstraintsType C;
	  //typedef typename GFS::template ConstraintsContainer<double>::Type C;

    
      //! \brief The domain type of the preconditioner.
      typedef X domain_type;
      //! \brief The range type of the preconditioner.
      typedef Y range_type;
      //! \brief The field type of the preconditioner.
      typedef typename X::ElementType field_type;

      // define the category
      enum {
        //! \brief The category the preconditioner is part of.
        category=Dune::SolverCategory::overlapping
      };


      /*! \brief Constructor.

        Constructor gets all parameters to operate the prec.
        \param gfs_ The grid function space.
        \param A_ The matrix to operate on.
      */
      OneLevel_OVLP_Schwarz (const GFS& gfs_, const M& J_, const C& c_)
        : gfs(gfs_),
          ranks(gfs.gridView().comm().size()),
          myRank(gfs.gridView().comm().rank()),
          part_unity(gfs,1.0),
          c(c_),
          rank_partition(gfs, myRank) //, solver(Dune::PDELab::Backend::native(A_),false) // this does the decomposition
      {

      	// ===

      	// Find neighbors (based on parallelhelper.hh in PDELab)

	    Dune::InterfaceType _interiorBorder_all_interface;

	    //! The actual communication interface used when algorithm requires All_All_Interface.
	    Dune::InterfaceType _all_all_interface;

	    // TODO: The following shortcut may never be fulfilled because we have no overlap?
	    // Let's try to be clever and reduce the communication overhead by picking the smallest
	    // possible communication interface depending on the overlap structure of the GFS.
	    // FIXME: Switch to simple comparison as soon as dune-grid:1b3e83ec0 is reliably available!
	    if (gfs.entitySet().partitions().value == Dune::Partitions::interiorBorder.value)
	      {
	        // The GFS only spans the interior and border partitions, so we can skip sending or
	        // receiving anything else.
	        _interiorBorder_all_interface = Dune::InteriorBorder_InteriorBorder_Interface;
	        _all_all_interface = Dune::InteriorBorder_InteriorBorder_Interface;
	      }
	    else
	      {
	        // In general, we have to transmit more.
	        _interiorBorder_all_interface = Dune::InteriorBorder_All_Interface;
	        _all_all_interface = Dune::All_All_Interface;
	      }
	    Dune::PDELab::DisjointPartitioningDataHandle<GFS,RankVector> pdh(gfs,rank_partition);
	    gfs.gridView().communicate(pdh,_interiorBorder_all_interface,Dune::ForwardCommunication);

	    std::set<RankIndex> rank_set;
	    for (RankIndex rank : rank_partition)
	      if (rank != myRank)
	        rank_set.insert(rank);

	    for (RankIndex rank : rank_set)
	      neighbor_ranks.push_back(rank);


      

        using Dune::PDELab::Backend::native;

        ISTLM J = Dune::PDELab::Backend::native(J_); // Unwrap J_ into Dune::BCRSMatrix<B>

        totalRows = J.N();

        subBlock_Size = totalRows / 4;

        A_subBlock_Size = 3 * subBlock_Size;

        typedef typename ISTLM::block_type B; // Block type of Jacobian - (Default Dune::FieldMatrix<double,1,1>)

        typedef typename Dune::BCRSMatrix<B>::CreateIterator Iter;

        typedef typename ISTLM::ConstRowIterator RowI;
        typedef typename ISTLM::ConstColIterator ColI;

        typedef typename ISTLX::Iterator iterator;

        // == Construct  A (Elasticity) SubMatrix of J

        Dune::BCRSMatrix<B> A(A_subBlock_Size,A_subBlock_Size,Dune::BCRSMatrix<B>::row_wise);

        std::vector<double> diag_for_SA(subBlock_Size);

        // == Sparsity Pattern

        for(Iter row= A.createbegin(); row!= A.createend(); ++row){
          for (int j = 0; j < A_subBlock_Size; j++){
            if( J.exists(row.index(),j)) {
              row.insert(j);
            }
          }
        }

        // == Add Values

        for(RowI row = A.begin(); row != A.end(); ++row){
            for(ColI col = row->begin(); col != row->end(); ++col){
              A[row.index()][col.index()] = J[row.index()][col.index()];
            }
        }

        // = Construct Coupling SubMatrix block of J

        L.setBuildMode(Dune::BCRSMatrix<B>::row_wise);
        L.setSize(A_subBlock_Size,subBlock_Size);

        // == Sparsity Pattern

        for(Iter row=L.createbegin(); row!=L.createend(); ++row){ // For each row 
          for (int j = 0; j < subBlock_Size; j++){
            if( J.exists(row.index(),j + A_subBlock_Size)) {
              row.insert(j);
            }
          }
        }

        // == Add Values

        for(RowI row = L.begin(); row != L.end(); ++row){
            for(ColI col = row->begin(); col != row->end(); ++col){
              L[row.index()][col.index()] = J[row.index()][col.index() + A_subBlock_Size];
              if (row.index() == col.index()){
                diag_for_SA[col.index()] += (1.0 / A[row.index()][row.index()]) * L[row.index()][col.index()] * L[row.index()][col.index()];
              }
            }
        }

        // = Construct Approximation Schur-Complement block of J

        Dune::BCRSMatrix<B> SA(subBlock_Size,subBlock_Size,Dune::BCRSMatrix<B>::row_wise);  

        // == Sparsity Pattern

        for(Iter row=SA.createbegin(); row!=SA.createend(); ++row){
          for (int j = 0; j < subBlock_Size; j++){
            if( J.exists(row.index() + A_subBlock_Size,j + A_subBlock_Size)) {
              row.insert(j);
            }
          }
        }

        // == Add Values

        for(RowI row = SA.begin(); row != SA.end(); ++row){
            for(ColI col = row->begin(); col != row->end(); ++col){
              SA[row.index()][col.index()] = J[row.index() + A_subBlock_Size][col.index() + A_subBlock_Size];
              if (row.index() == col.index()){
                SA[row.index()][row.index()] += diag_for_SA[row.index()];
              }
            }
        }

        //  == Set matrices for subdomain solver

        solver.setMatrix(J);

        solver_A.setMatrix(A);

        solver_SA.setMatrix(SA);

        //  ======= Construct Partion of Unity

        if (myRank == 0 && configuration.get<bool>("verbosity",false)){

          std::cout << std::endl << "Constructing Partition of Unity" << std::endl;
        }

        Dune::PDELab::set_constrained_dofs(c,0.0,part_unity); // Zero on subdomain boundary and Dirichlet boundary to zero

        Dune::PDELab::AddDataHandle<GFS,X> parth(gfs, part_unity);

        if (ranks > 1){ // If more than one processor - common
          gfs.gridView().communicate(parth,Dune::All_All_Interface,Dune::ForwardCommunication); // Send to all and receive from call
        }

        Dune::PDELab::set_constrained_dofs(c,0.0,part_unity); // Zero on subdomain boundary (Need that a 2nd time due to add comm before!)

        for (auto iter = part_unity.begin(); iter != part_unity.end(); iter++) {
            if (*iter > 0){ *iter = 1.0 / *iter; }
        }

        if (myRank == 0 && configuration.get<bool>("verbosity",false)){
          std::cout << "COMPLETED!" << std::endl;
        }

        // === Construct Global Basis

        int nev = configuration.get<int>("eigenvectors",0); // Number of Eigenvalues to be used on processor

        if (nev == 0) { // Only partition of unity as coarse space
          local_base.resize(1);
          local_base[0] = std::make_shared<X>(part_unity);
        }

        // Normalize basis vectors
        for (int i = 0; i < local_base.size(); i++) {
          native(*(local_base[i])) *= 1.0 / (native(*(local_base[i])) * native(*(local_base[i])));
        }

        // Communicate local coarse space dimensions
        int buf_base_sizes[ranks];
        int local_size = local_base.size();
        MPI_Allgather(&local_size, 1, MPI_INT, &buf_base_sizes, 1, MPI_INT, gfs.gridView().comm());
        local_base_sizes = std::vector<int>(buf_base_sizes, buf_base_sizes + ranks);

        // Count coarse space dimensions
        global_base_size = 0;
        for (int n : local_base_sizes) {
          global_base_size += n;
        }

         // Compute maximum local basis size over all ranks
    	int max_local_base_size = 0;
    	for (int rank = 0; rank < ranks; rank++) {
      		if (local_base_sizes[rank] > max_local_base_size){
        		max_local_base_size = local_base_sizes[rank];
        	}
    	}

        // Compute dof offset for this processor
        my_basis_array_offset = 0;
        for (int i = 0; i < myRank; i++) {
          my_basis_array_offset += local_base_sizes[i];
        }

         if(myRank == 0 && configuration.get<bool>("verbosity",false)){
          std::cout << "=== Constructing Coarse Matrix .... ";
         }

         coarse_system_u = std::make_shared<COARSE_M>(global_base_size, global_base_size, COARSE_M::row_wise); 
         coarse_system_p = std::make_shared<COARSE_M>(global_base_size, global_base_size, COARSE_M::row_wise);

         std::vector<std::vector<std::vector<double> > > local_rows_u(local_base_sizes[myRank]);
         std::vector<std::vector<std::vector<double> > > local_rows_p(local_base_sizes[myRank]);

         // For each of basis on this processor
    	 for (int basis_index = 0; basis_index < local_base_sizes[myRank]; basis_index++) { 
      		local_rows_u[basis_index].resize(neighbor_ranks.size()+1); // Space for each neighbour + this one
      		local_rows_p[basis_index].resize(neighbor_ranks.size()+1);
    	 }

    	 for (int basis_index_remote = 0; basis_index_remote < max_local_base_size; basis_index_remote++) {
    	 // For basis less than maximum possible

    	 	// Vector of Xs of each neighbour processor
    	 	std::vector< std::shared_ptr<X> > neighbor_basis(neighbor_ranks.size());
      		
    	 	// For each neighbour initial 
      		for (int i = 0; i < neighbor_basis.size(); i++) {
        		neighbor_basis[i] = std::make_shared<X>(gfs, 0.0); // Initialise neighbour basis
      		}

      		if (basis_index_remote < local_base_sizes[myRank]) { // If remote index is less than local_base_sizes[my_rank]
        		Dune::MultiCommDataHandle<GFS,X,RankIndex,4> commdh(gfs, *local_base[basis_index_remote], neighbor_basis, neighbor_ranks);
        		gfs.gridView().communicate(commdh,Dune::All_All_Interface,Dune::ForwardCommunication);
      		} 
		    else {
		        X dummy(gfs, 0.0);
		        Dune::MultiCommDataHandle<GFS,X,RankIndex,4> commdh(gfs, dummy, neighbor_basis, neighbor_ranks);
		        gfs.gridView().communicate(commdh,Dune::All_All_Interface,Dune::ForwardCommunication);
		    }

		    if (basis_index_remote < local_base_sizes[myRank]) {

		    	auto base_vector = *local_base[basis_index_remote];

		    	ISTLX Atimesv(A_subBlock_Size), base_vector_u(A_subBlock_Size);
		    	ISTLX SAtimesv(subBlock_Size), base_vector_p(subBlock_Size);

		    	for (iterator row=native(base_vector).begin(); row!=native(base_vector).end(); ++row){
          			if (row.index() < A_subBlock_Size){
            			base_vector_u[row.index()] = native(base_vector)[row.index()];
          			}
          			else{
            			base_vector_p[row.index() - A_subBlock_Size] = native(base_vector)[row.index()];
          			}
        		} // Split up local basis functions

        		A.mv(base_vector_u,Atimesv);
        		SA.mv(base_vector_p,SAtimesv);

        		for (int basis_index = 0; basis_index < local_base_sizes[myRank]; basis_index++){

        			for (iterator row=native(base_vector).begin(); row!=native(base_vector).end(); ++row){
	          			if (row.index() < A_subBlock_Size){
	            			base_vector_u[row.index()] = native(*local_base[basis_index])[row.index()];
	          			}
	          			else{
	            			base_vector_p[row.index() - A_subBlock_Size] = native(*local_base[basis_index])[row.index()];
	          			}
        			} // Split up local basis functions
        			double entry = base_vector_u * Atimesv;
        			local_rows_u[basis_index][neighbor_ranks.size()].push_back(entry);

        			entry = base_vector_p * SAtimesv;
        			local_rows_p[basis_index][neighbor_ranks.size()].push_back(entry);

        		}

        		} // end if basis_index_remote < local_base_sizes[myRank]

        		for (int neighbor_id = 0; neighbor_id < neighbor_ranks.size(); neighbor_id++) {// For each neighbour
			        if (basis_index_remote >= local_base_sizes[neighbor_ranks[neighbor_id]])
			          continue;

			        auto base_vector = *neighbor_basis[neighbor_id];

			        ISTLX Atimesv(A_subBlock_Size), base_vector_u(A_subBlock_Size);
		    		  ISTLX SAtimesv(subBlock_Size), base_vector_p(subBlock_Size);

		    		for (iterator row=native(base_vector).begin(); row!=native(base_vector).end(); ++row){
          				if (row.index() < A_subBlock_Size){
            				base_vector_u[row.index()] = native(base_vector)[row.index()];
          				}
          				else{
            			base_vector_p[row.index() - A_subBlock_Size] = native(base_vector)[row.index()];
          				}
        			} // Split up local basis functions

        			A.mv(base_vector_u,Atimesv);
        			SA.mv(base_vector_p,SAtimesv);

					for (int basis_index = 0; basis_index < local_base_sizes[myRank]; basis_index++){

        			for (iterator row=native(base_vector).begin(); row!=native(base_vector).end(); ++row){
	          			if (row.index() < A_subBlock_Size){
	            			base_vector_u[row.index()] = native(*local_base[basis_index])[row.index()];
	          			}
	          			else{
	            			base_vector_p[row.index() - A_subBlock_Size] = native(*local_base[basis_index])[row.index()];
	          			}
        			} // Split up local basis functions
        			double entry = base_vector_u * Atimesv;
        			local_rows_u[basis_index][neighbor_id].push_back(entry);

        			entry = base_vector_p * SAtimesv;
        			local_rows_p[basis_index][neighbor_id].push_back(entry);

        			}

      			}	

    	 }

    	 // Start building the coarse matrix
    	 auto setup_row_u = coarse_system_u->createbegin();	
    	 auto setup_row_p = coarse_system_p->createbegin();

    	 int row_id = 0;


    	 for (int rank = 0; rank < ranks; rank++) {
      		for (int base_index = 0; base_index < local_base_sizes[rank]; base_index++) {

      			// Communicate number of entries in this row
        		int couplings = 0;
		        if (rank == myRank) {
		          couplings = local_base_sizes[myRank];
		          for (int neighbor_id : neighbor_ranks) {
		            couplings += local_base_sizes[neighbor_id];
		          }
		        }
		        MPI_Bcast(&couplings, 1, MPI_INT, rank, gfs.gridView().comm());


		        // Communicate row's pattern
        		int entries_pos[couplings];



		        
		        if (rank == myRank) {
		          int cnt = 0;
		          for (int base_index2 = 0; base_index2 < local_base_sizes[myRank]; base_index2++) {
		            entries_pos[cnt] = my_basis_array_offset + base_index2;
		            cnt++;
		          }

		          for (int neighbor_id = 0; neighbor_id < neighbor_ranks.size(); neighbor_id++) {
		            int neighbor_offset = basis_array_offset (neighbor_ranks[neighbor_id]);
		            
		            for (int base_index2 = 0; base_index2 < local_base_sizes[neighbor_ranks[neighbor_id]]; base_index2++) {
		              entries_pos[cnt] = neighbor_offset + base_index2;
		              cnt++;
		            }
		          }
		        }

		    
		        MPI_Bcast(&entries_pos, couplings, MPI_INT, rank, gfs.gridView().comm());

		        // Communicate actual entries
		        double entries_u[couplings], entries_p[couplings];
		        if (rank == myRank) {
		          int cnt = 0;
		          for (int base_index2 = 0; base_index2 < local_base_sizes[myRank]; base_index2++) {
		            entries_u[cnt] = local_rows_u[base_index][neighbor_ranks.size()][base_index2];
		            entries_p[cnt] = local_rows_p[base_index][neighbor_ranks.size()][base_index2];
		            cnt++;
		          }
		          for (int neighbor_id = 0; neighbor_id < neighbor_ranks.size(); neighbor_id++) {
		            int neighbor_offset = basis_array_offset (neighbor_ranks[neighbor_id]);
		            for (int base_index2 = 0; base_index2 < local_base_sizes[neighbor_ranks[neighbor_id]]; base_index2++) {
		              entries_u[cnt] = local_rows_u[base_index][neighbor_id][base_index2];
		              entries_p[cnt] = local_rows_p[base_index][neighbor_id][base_index2];
		              cnt++;
		            }
		          }
		        }
		        MPI_Bcast(&entries_u, couplings, MPI_DOUBLE, rank, gfs.gridView().comm());
		        MPI_Bcast(&entries_p, couplings, MPI_DOUBLE, rank, gfs.gridView().comm());

		        // Build matrix row based on pattern
		        for (int i = 0; i < couplings; i++){
		          setup_row_u.insert(entries_pos[i]);
		      	  setup_row_p.insert(entries_pos[i]);
		      	}
		        ++setup_row_u;
		        ++setup_row_p;

		        // Set matrix entries
        		for (int i = 0; i < couplings; i++) {
         		//if (my_rank == 0)
          		//  std::cout << "rank " << my_rank << " setting row " << row_id << ", col " << entries_pos[i] << " to " << entries[i] << std::endl;
          			(*coarse_system_u)[row_id][entries_pos[i]] = entries_u[i];
          			(*coarse_system_p)[row_id][entries_pos[i]] = entries_p[i];
        		}

        		row_id++;


      		}
      	}

      	if (myRank == 0 & configuration.get<bool>("verbosity",false)){
	         std::cout << "COMPLETED!" << std::endl;
	       }


    	 coarse_solver_u = std::make_shared<Dune::UMFPack<COARSE_M> >(*coarse_system_u);
    	 coarse_solver_p = std::make_shared<Dune::UMFPack<COARSE_M> >(*coarse_system_p);


      }

      int basis_array_offset (int rank) {
    	int offset = 0;
    	for (int i = 0; i < rank; i++) {
      		offset += local_base_sizes[i];
    	}
    	return offset;
  	  }

  	 std::shared_ptr<COARSE_V> restrict_defect (const X& d, int option = 1) const {

    	using Dune::PDELab::Backend::native;
    	
      std::shared_ptr<COARSE_V> coarse_defect = std::make_shared<COARSE_V>(global_base_size,global_base_size);

        int recvcounts[ranks];
        int displs[ranks];
        for (int rank = 0; rank < ranks; rank++) {
          displs[rank] = 0;
        }
        for (int rank = 0; rank < ranks; rank++) {
          recvcounts[rank] = local_base_sizes[rank];
          for (int i = rank+1; i < ranks; i++)
            displs[i] += local_base_sizes[rank];
        }

        double buf_defect[global_base_size];
        double buf_defect_local[local_base_sizes[myRank]];

        int lmin = 0, lmax = A_subBlock_Size; 
        if(option > 1){lmin = A_subBlock_Size; lmax = native(d).N();}
        

        for (int base_index = 0; base_index < local_base_sizes[myRank]; base_index++) {
          buf_defect_local[base_index] = 0.0;
              for (int i = lmin; i < lmin; i++){
                buf_defect_local[base_index] += native(*local_base[base_index])[i] * native(d)[i];
              }
        }

        MPI_Allgatherv(&buf_defect_local, local_base_sizes[myRank], MPI_DOUBLE, &buf_defect, recvcounts, displs, MPI_DOUBLE, gfs.gridView().comm());
        for (int base_index = 0; base_index < global_base_size; base_index++) {
          (*coarse_defect)[base_index] = buf_defect[base_index];
        }

    


	    return coarse_defect;
  	}

  std::shared_ptr<X> prolongate_defect (const COARSE_V& v0, int option = 0) const {
    using Dune::PDELab::Backend::native;
    auto v = std::make_shared<X>(gfs, 0.0);

    int lmin = A_subBlock_Size; int lmax = A_subBlock_Size + subBlock_Size;
      if(option > 1){ lmin = 0; lmax = A_subBlock_Size;  }

    // Prolongate result
    for (int base_index = 0; base_index < local_base_sizes[myRank]; base_index++) {
      X local_result(*local_base[base_index]);

      if(option > 0){

        typedef typename ISTLX::Iterator iterator;
        for (iterator row=native(local_result).begin(); row!=native(local_result).end(); ++row){
          if (row.index() >= lmin && row.index() < lmax){
            native(local_result)[row.index()] = 0.0;
          }    
        }

      } // if option is greater than zero

      native(local_result) *= v0[my_basis_array_offset + base_index];
      *v += local_result;
    }
    return v;
  }






      /*!
        \brief Prepare the preconditioner.
      */
      virtual void pre (X& x, Y& b) {}

      /*!
        \brief Apply the precondioner.
      */
      virtual void apply (X& v, const Y& d)
      {

        using Dune::PDELab::Backend::native;

        Dune::InverseOperatorResult stat;
        Y b(d); // need copy, since solver overwrites right hand side

        ISTLX v_u(A_subBlock_Size), v_p(subBlock_Size);
        ISTLX b_u(A_subBlock_Size), b_p(subBlock_Size);

        typedef typename ISTLX::Iterator iterator;
        for (iterator row=native(b).begin(); row!=native(b).end(); ++row){
          if (row.index() < A_subBlock_Size){
            v_u[row.index()] = native(v)[row.index()];
            b_u[row.index()] = native(b)[row.index()];
          }
          else{
            v_p[row.index() - A_subBlock_Size] = native(v)[row.index()];
            b_p[row.index() - A_subBlock_Size] = native(b)[row.index()];
          }
        }

        int option = configuration.get<int>("preconditioner_option",1);

        if (option == 0){
          solver.apply(native(v),native(b),stat);
        }
        else if (option == 1){
          solver_SA.apply(v_p,b_p,stat); // Compute the inverse of approximation to Schur Complement, SA on subdomain
          L.mmv(v_p,b_u);
          solver_A.apply(v_u,b_u,stat); // Compute the inverse of A on subdomain
        }
        else{
          solver_A.apply(v_u,b_u,stat); // Compute the inverse of A on subdomain
          L.umtv(v_u,b_p);
          solver_SA.apply(v_p,b_p,stat); // Compute the inverse of approximation to Schur Complement, SA on subdomain
        }
        // Collection solutions back into vector 



       if (option > 0){

       for (iterator row=native(v).begin(); row!=native(v).end(); ++row){
          if (row.index() < A_subBlock_Size){
            native(v)[row.index()] = v_u[row.index()];
          }
          else{
            native(v)[row.index()] = v_p[row.index() - A_subBlock_Size];
          }
        }
      }


      if (!configuration.get<bool>("coarseSpaceActive",false)) {
    

        if (gfs.gridView().comm().size()>1){
            Dune::PDELab::AddDataHandle<GFS,X> adddh(gfs,v);
            gfs.gridView().communicate(adddh,Dune::All_All_Interface,Dune::ForwardCommunication);
        }

      } // end if coarseSpace not Active
      else{

        // Compute the Coarse Defect for the pressure

        auto coarse_defect_p = restrict_defect(d,2);

        Dune::InverseOperatorResult result;
        COARSE_V v0(global_base_size,global_base_size);
        coarse_solver_p->apply(v0, *coarse_defect_p, result);

        auto coarse_corrector_p = prolongate_defect (v0,2);

        ISTLX defect_u(A_subBlock_Size);

        typedef typename ISTLX::Iterator iterator;

        for (iterator row=native(b).begin(); row!=native(b).end(); ++row){
          if (row.index() < A_subBlock_Size){
            defect_u[row.index()] = native(d)[row.index()];
          }
        }

        L.mmv(native(*coarse_corrector_p),defect_u);

        for (iterator row=native(b).begin(); row!=native(b).end(); ++row){
          if (row.index() < A_subBlock_Size){
            native(b)[row.index()] = defect_u[row.index()];
          }
          else{native(b)[row.index()]=0.0;}
        }

        auto coarse_defect_u = restrict_defect(b,2);

        COARSE_V v1(global_base_size,global_base_size);

        coarse_solver_u->apply(v1, *coarse_defect_u,result);

        auto coarse_corrector_u = prolongate_defect(v1,1);

        v += *coarse_corrector_p;
        v += *coarse_corrector_u;

        Dune::PDELab::AddDataHandle<GFS,X> result_addh(gfs,v);
        gfs.gridView().communicate(result_addh,Dune::All_All_Interface,Dune::ForwardCommunication);

      } // End if Coarse Space is Active

    } // end apply preconditioner!
  
      /*!
        \brief Clean up.
      */
      virtual void post (X& x) {}

    private:
      const GFS& gfs;
      Dune::UMFPack<ISTLM> solver;
      Dune::UMFPack<ISTLM> solver_A;
      Dune::UMFPack<ISTLM> solver_SA;
      ISTLM L;
      int totalRows, subBlock_Size, A_subBlock_Size;
      int ranks, myRank;
      X part_unity;
      const C& c;
      std::vector<std::shared_ptr<X> > local_base;
      std::vector<ISTLX> local_base_u;
      std::vector<ISTLX> local_base_p;
      std::vector<int> local_base_sizes; // Dimensions of local coarse space per subdomain

      RankVector rank_partition; // vector to identify unique decomposition
  	  std::vector<int> neighbor_ranks;

      int my_basis_array_offset; // Start of local basis functions in a consecutive global ordering
      int global_base_size; // Dimension of entire coarse space


      //RankVector rank_partition; // vector to identify unique decomposition
  	 // std::vector<RankIndex> neighbor_ranks;

      std::shared_ptr<COARSE_M> coarse_system_u; // Coarse space matrix
      std::shared_ptr<COARSE_M> coarse_system_p; // Coarse space matrix

  	public:
  		std::shared_ptr<Dune::UMFPack<COARSE_M> > coarse_solver_u;
  		std::shared_ptr<Dune::UMFPack<COARSE_M> > coarse_solver_p;

    };
#endif



 template<class GFS, class C, template<typename> class Solver>
    class OVLP_Schwarz_Base
      : public Dune::PDELab::OVLPScalarProductImplementation<GFS>, public Dune::PDELab::LinearResultStorage
    {
    public:
      /*! \brief make a linear solver object

        \param[in] gfs_ a grid function space
        \param[in] c_ a constraints object
        \param[in] maxiter_ maximum number of iterations to do
        \param[in] verbose_ print messages if true
      */
      OVLP_Schwarz_Base (const GFS& gfs_, const C& c_, unsigned maxiter_=5000,
                                              int verbose_=1)
        : Dune::PDELab::OVLPScalarProductImplementation<GFS>(gfs_), gfs(gfs_), c(c_), maxiter(maxiter_), verbose(verbose_)
      {}

      template<class M, class V, class W>
      void apply(M& A, V& z, W& r, typename Dune::template FieldTraits<typename V::ElementType >::real_type reduction)
      {
        typedef Dune::PDELab::OverlappingOperator<C,M,V,W> POP;
          POP pop(c,A);
        typedef Dune::PDELab::OVLPScalarProduct<GFS,V> PSP;
          PSP psp(*this);

        typedef OneLevel_OVLP_Schwarz<GFS,M,V,W,C> PREC;
        PREC prec(gfs,A,c);
        int verb=0;
        if (gfs.gridView().comm().rank()==0) verb=verbose;
        Solver<V> solver(pop,psp,prec,reduction,maxiter,verb);
        Dune::InverseOperatorResult stat;
        solver.apply(z,r,stat);
        res.converged  = stat.converged;
        res.iterations = stat.iterations;
        res.elapsed    = stat.elapsed;
        res.reduction  = stat.reduction;
        res.conv_rate  = stat.conv_rate;
      }

    private:
      const GFS& gfs;
      const C& c;
      unsigned maxiter;
      int verbose;
    };


  //! \addtogroup PDELab_ovlpsolvers Overlapping Solvers
    //! \{
    /**
     * @brief Overlapping parallel BiCGStab solver with SuperLU preconditioner
     * @tparam GFS The Type of the GridFunctionSpace.
     * @tparam CC The Type of the Constraints Container.
     */
    template<class GFS, class CC>
    class OVLP_BCGS_UMFPACK
      : public OVLP_Schwarz_Base<GFS,CC,Dune::BiCGSTABSolver>
    {
    public:

      /*! \brief make a linear solver object

        \param[in] gfs_ a grid function space
        \param[in] cc_ a constraints container object
        \param[in] maxiter_ maximum number of iterations to do
        \param[in] verbose_ print messages if true
      */
      OVLP_BCGS_UMFPACK (const GFS& gfs_, const CC& cc_, unsigned maxiter_=5000,
                                              int verbose_=1)
        : OVLP_Schwarz_Base<GFS,CC,Dune::BiCGSTABSolver>(gfs_,cc_,maxiter_,verbose_)
      {}
    };