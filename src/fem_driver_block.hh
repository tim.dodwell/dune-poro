#include "boundaryConditions.hh"
#include "poro.hh"
#include "poro_time.hh"
#include "backwardEuler.hh"
#include "prec.hh"

template <typename MODEL, typename GRID, typename HELPER>
void fem_driver_block(MODEL& model, const GRID& grid, HELPER& helper,bool verb = true){

    double time = 0.0; // Initialise time

    double dt = model.get_dt(); // Set time stepper from model class

    // Get leafGridView from grid
    typedef typename GRID::LeafGridView GV;
    GV gv = grid.leafGridView();

  	const int dim = GV::Grid::dimension;
    typedef typename GV::Grid::ctype Coord;

	 // Construct FEM Spaces
    typedef Dune::PDELab::QkLocalFiniteElementMap<GV,typename GV::Grid::ctype,double,1> FEM_P2;
        FEM_P2 P2(gv);
	  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,typename GV::Grid::ctype,double,1> FEM_P1;
		    FEM_P1 P1(gv);

	// Construct grid function spaces for each degree of freedom

  // Construct grid function spaces for each degree of freedom
  typedef Dune::PDELab::OverlappingConformingDirichletConstraints CON;
    CON con;

    typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::none,1> VBE;

    typedef Dune::PDELab::GridFunctionSpace<GV, FEM_P1, CON, VBE> P1_GFS;
    typedef Dune::PDELab::GridFunctionSpace<GV, FEM_P2, CON, VBE> P2_GFS;

		P2_GFS dispU(gv,P2); dispU.name("U");
		P2_GFS dispV(gv,P2); dispV.name("V");
    P2_GFS dispW(gv,P2); dispW.name("W");
		
    P1_GFS pressure(gv,P1); pressure.name("PRESSURE");

  // Construct a Composite Grid Function Space with Lexiographic Ordering (i.e. all dofs for child 0 first, then child 1 and so on)
	typedef Dune::PDELab::CompositeGridFunctionSpace <VBE,Dune::PDELab::LexicographicOrderingTag, P2_GFS, P2_GFS, P2_GFS,P1_GFS> GFS;	
    GFS gfs(dispU, dispV, dispW,pressure);

  
 // Make constraints map and initialize it from a function
	typedef typename GFS::template ConstraintsContainer<double>::Type C;
		C cg, cg_ext;
  cg.clear();
  cg_ext.clear();

	typedef Scalar_BC<GV,double,MODEL> BC;
		BC U_cc(gv,model), V_cc(gv,model), W_cc(gv,model),p_cc(gv,model);
        U_cc.setDof(1); 
        V_cc.setDof(2); 
        W_cc.setDof(3); 
        p_cc.setDof(4); 

	typedef Dune::PDELab::CompositeConstraintsParameters<BC,BC,BC,BC> Constraints;
    Constraints constraints(U_cc,V_cc,W_cc,p_cc);

	Dune::PDELab::constraints(constraints,gfs,cg);

  if (configuration.get<bool>("EVboundariesFromProblem", ""))
    Dune::PDELab::constraints_exterior(constraints,gfs,cg_ext);

  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
    MBE mbe(402); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().//


  typedef PoroElasticity_K<MODEL,24,8> K_LOP;
    K_LOP k_lop(model,dt);

  typedef Dune::PDELab::GridOperator<GFS,GFS,K_LOP,MBE,double,double,double,C,C> K_GO;
   K_GO k_go(gfs,cg,gfs,cg,k_lop,mbe);


  typedef PoroElasticity_M<MODEL,24,8> M_LOP;
    M_LOP m_lop(model,dt);

  typedef Dune::PDELab::GridOperator<GFS,GFS,M_LOP,MBE,double,double,double,C,C> M_GO;
   M_GO m_go(gfs,cg,gfs,cg,m_lop,mbe);

  // << Make FE function with initial values >>
  typedef typename K_GO::Traits::Domain X;
    X x(gfs,0.0);

  BC u(gv,model), v(gv,model), w(gv,model), p(gv,model);
    u.setDof(1);  v.setDof(2);  w.setDof(3), p.setDof(4);

  typedef Dune::PDELab::CompositeGridFunction<BC,BC,BC,BC> InitialSolution;
    InitialSolution initial_solution(u,v,w,p);

  Dune::PDELab::interpolate(initial_solution,gfs,x);

  //Set up Grid operator for Jacobian
  typedef typename K_GO::Jacobian J_K;

  //  Matrix with "correct" boundary conditions
    K_GO go(gfs,cg,gfs,cg,k_lop,mbe);
    J_K K(go);
    K = 0.0;
    go.jacobian(x,K);

  // Construct Block-Preconditioner     

  typedef Prec_K<MODEL,24,8> BP_LOP;
    BP_LOP bp_lop(model,dt);

  typedef Dune::PDELab::GridOperator<GFS,GFS,BP_LOP,MBE,double,double,double,C,C> BP_GO;

  typedef typename BP_GO::Jacobian J;

  //  Matrix with Dirichlet Boundary Conditions on boundary data and processor boundaries
    BP_GO bp_go(gfs,cg,gfs,cg,bp_lop,mbe);
    J PC(bp_go);
    PC = 0.0;
    bp_go.jacobian(x,PC);

  // Construct with Dirichlet Condition on nodes with boudnary data - Neumann on Processor Boundary

   BP_GO go_bp_neu(gfs,cg_ext,gfs,cg_ext,bp_lop,mbe);
   J PC_neu(go_bp_neu);
   PC_neu = 0.0;
   go_bp_neu.jacobian(x,PC_neu);
 
   //Create local operator on overlap
   //typedef Dune::PDELab::LocalOperatorOvlpRegion<K_LOP, GFS> LOP_ovlp;
   //LOP_ovlp lop_ovlp(k_lop,gfs);
    typedef Dune::PDELab::LocalOperatorOvlpRegion<BP_LOP, GFS> LOP_ovlp;
      LOP_ovlp lop_ovlp(bp_lop,gfs);
    
    typedef Dune::PDELab::GridOperator<GFS,GFS,LOP_ovlp,MBE,double,double,double,C,C> GO_ovlp;
        GO_ovlp go_ovlp(gfs,cg_ext,gfs,cg_ext,lop_ovlp,mbe);
        
    typedef typename GO_ovlp::Jacobian J2;
      J2 PC_ovlp(go_ovlp);
    
    PC_ovlp = 0.0;
    go_ovlp.jacobian(x,PC_ovlp);

    //Set up solution vector and some necessary operators
   
    typedef Dune::PDELab::OverlappingOperator<C,J_K,X,X> POP;
      POP popf(cg,K);
    
    typedef Dune::PDELab::istl::ParallelHelper<GFS> PIH;
      PIH pihf(gfs);
        
    typedef Dune::PDELab::OverlappingScalarProduct<GFS,X> OSP;
      OSP ospf(gfs,pihf);


    // Setup GENEO Preconditioner
    typedef Dune::PDELab::LocalFunctionSpace<GFS, Dune::PDELab::AnySpaceTag> LFSU;
    typedef typename LFSU::template Child<0>::Type LFS;
        LFSU lfsu(gfs);
        LFS lfs = lfsu.template child<0>();
        
    typedef TwoLevelOverlappingSchwarzDistributedCoarseGrid<GFS,LFS,C,J,X,X,4> PREC;
        //PREC prec(gfs, lfs, cg, cg_ext, AF, AF_neu, PC_ovlp,PC,1e-5); // threshold not set
        PREC prec(gfs, lfs, cg, cg_ext, PC, PC_neu, PC_ovlp,PC_neu,1e-5); // threshold not set

    //typedef Dune::BiCGSTABSolver<X> LS;
    //LS ls(popf,ospf,prec,configuration.get<double>("linear_solver_tolerance",1e-8),configuration.get<int>("linear_solver_max_iterations",5000),configuration.get<bool>("solver_verbose",true));
    typedef Dune::RestartedGMResSolver<X> LS;
    LS ls(popf,ospf,prec,configuration.get<double>("linear_solver_tolerance",1e-8),configuration.get<double>("gmres_restart",100),configuration.get<int>("linear_solver_max_iterations",5000),configuration.get<bool>("solver_verbose",true));


    //typedef Dune::CGSolverFork<X> LS;
      //LS ls(popf,ospf,prec,configuration.get<double>("linear_solver_tolerance",1e-8),configuration.get<int>("linear_solver_max_iterations",5000),configuration.get<bool>("solver_verbose",true));
    
    // Solve Problem

    std::cout << "Do we get here either?" << std::endl;

    X xnew(gfs,0.0);

    Dune::PDELab::interpolate(initial_solution,gfs,xnew);

    X z(gfs,0.0); // Setup up corrector vector
    X d(gfs,0.0); // Container for the defect

    go.residual(xnew,d);

    X M_x0(gfs,0.0); m_go.residual(x,M_x0);

    d -= M_x0;

    Dune::InverseOperatorResult result;

    ls.apply(z,d,result); // linear solve

    xnew -= z; // Update the solution

    int tstep = 0;

    stringstream sstm;
    sstm << "Solution_" << tstep;
    string file_output_name = sstm.str();

    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,0);
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,xnew);
    vtkwriter.write(file_output_name,Dune::VTK::appendedraw);


    std::cout << "What happens here!" << std::endl;
    




  /*typedef BackwardEuler<K_GO,M_GO,LS,X> BE;
      BE bE(k_go,m_go,ls,model.getReduction());



    // << Time integration loop >>
      X xnew(gfs,0.0);
    Dune::PDELab::interpolate(initial_solution,gfs,xnew);

    int tstep = 0;

   while (time < 10.0 ){

      tstep += 1; // Increment timestep

      time += dt; // Increment time

      std::cout << "This is tstep = " << tstep << std::endl;

      bE.apply(x,xnew); // Apply backward Euler Step

      X res(gfs,0.0);

      m_go.residual(x,res);

      std::cout << "Residual = " << ls.norm(res) << std::endl;

      std::cout << "=================" << std::endl;

      if (verb){

        stringstream sstm;
        sstm << "Solution_" << tstep;
        string file_output_name = sstm.str();

        Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,0);
        Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,xnew);
        vtkwriter.write(file_output_name,Dune::VTK::appendedraw);
      }

      x = xnew;


   }*/



} // End Cosserat_driver
