#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>

//#include "defaultimp.hh"
//#include "pattern.hh"
//#include "flags.hh"
//#include "idefault.hh"

using namespace std;

        
template<typename PARAM, int udof, int pdof>
class Prec_K:
        //public Dune::PDELab::NumericalJacobianApplyVolume<Prec_K<PARAM,udof,pdof>>,
        //public Dune::PDELab::NumericalJacobianVolume<Prec_K<PARAM,udof,pdof>>,
        public Dune::PDELab::FullVolumePattern,
        public Dune::PDELab::LocalOperatorDefaultFlags
{
public:
// pattern assembly flags
    enum { doPatternVolume = true };
            
// residual assembly flags
    enum { doAlphaVolume = true };
    enum { doLambdaBoundary = true };
    
Prec_K (const PARAM& param_, double dt_, unsigned int intorder_=2):param(param_), intorder(intorder_), dt(dt_){}


template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, M & mat) const
{

    // Galerkin Finite Elements - assumes lfsu == lfsv
    const int dim = 3;

    // Unwrap shape local function spaces
    typedef typename LFSU::template Child<0>::Type LFSU_U;
    typedef typename LFSU::template Child<1>::Type LFSU_V;
    typedef typename LFSU::template Child<2>::Type LFSU_W;
    typedef typename LFSU::template Child<3>::Type LFSU_P;
              
    const LFSU_U& lfsu_u = lfsu.template child<0>();
    const LFSU_V& lfsu_v = lfsu.template child<1>();
    const LFSU_W& lfsu_w = lfsu.template child<2>();
    const LFSU_P& lfsu_p = lfsu.template child<3>();

    const unsigned int usize = lfsu_u.size();

    const unsigned int psize = lfsu_p.size();


    // domain and range field type
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainType D;
    typedef typename M::value_type RF;
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LFSU_U::Traits::SizeType size_type;
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeType;
    typedef typename LFSU_P::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeType_P;
                
    // select quadrature rule
    auto geo = eg.geometry();
    
    const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(geo.type(),intorder);
                
    // Evaluate Cosserat tensor (assumes it is constant over a single element)
    Dune::FieldMatrix<double,6,6> C = param.ElasticTensor();

    RF dt_k = dt * param.Permeability();
    
    
    // Loop over quadrature points
    for (const auto& ip : rule)
    {

        // geometric weight
        RF factor = geo.integrationElement(ip.position()) * ip.weight();
        
        // Evaluate shape functions at Integration Point
        std::vector<RangeType> phi(usize);    
        lfsu_u.finiteElement().localBasis().evaluateFunction(ip.position(),phi);
        
        // Evaluate gradients of shape function at integration point
        std::vector<JacobianType> js(usize);
        lfsu_u.finiteElement().localBasis().evaluateJacobian(ip.position(),js);
        
        // transform gradient to real element
        const typename EG::Geometry::JacobianInverseTransposed jac = geo.jacobianInverseTransposed(ip.position());
        
        std::vector<Dune::FieldVector<RF,dim> > gradphi(usize);
            for (size_t i=0; i<usize; i++){
                gradphi[i] = 0.0;
                jac.umv(js[i][0],gradphi[i]);
            }     

        // Compute B - such that Strain = B * d - Checked
        Dune::FieldMatrix<double,6,udof> B(0.0);
        for (size_type i = 0; i < usize; i++){
            B[0][i] = gradphi[i][0]; // E11
            B[1][i + usize] = gradphi[i][1]; // E22
            B[2][i + 2 * usize] = gradphi[i][2]; // E33
            B[3][i + usize] = gradphi[i][2]; B[3][i + 2 * usize] = gradphi[i][1]; // E23  = U2,3 + U3,2
            B[4][i] = gradphi[i][2]; B[4][i + 2 * usize] = gradphi[i][0]; // E13  = U1,3 + U3,1
            B[5][i] = gradphi[i][1]; B[5][i + usize] = gradphi[i][0]; // E12  = U1,2 + U2,1           
        }

        Dune::FieldMatrix<double,6,udof> CB = C.rightmultiplyany(B);
          Dune::FieldMatrix<double,udof,6> BT(0.0);
          for (int i = 0; i < udof; i++){
            for (int j = 0; j < 6; j++){
              BT[i][j] = B[j][i];
            }
          }

          // Error appears to be in A matrix

          Dune::FieldMatrix<double,udof,udof> A = BT.rightmultiplyany(CB);

          A *= factor;

          for (int i = 0; i < usize; i++){
            for (int j = 0; j < usize; j++){

              mat.accumulate(lfsu_u , i , lfsu_u , j, 0.5*(A[i][j] + A[j][i]));
              mat.accumulate(lfsu_u , i , lfsu_v , j, 0.5*(A[i][j + usize] + A[j + usize][i]));
              mat.accumulate(lfsu_u , i , lfsu_w , j, 0.5*(A[i][j + 2*usize] + A[j + 2* usize][i]));
              mat.accumulate(lfsu_v , i , lfsu_u , j, 0.5*(A[i][j + usize] + A[j + usize][i]));
              mat.accumulate(lfsu_w , i , lfsu_u , j, 0.5*(A[i][j + 2*usize] + A[j + 2*usize][i]));

              mat.accumulate(lfsu_v , i , lfsu_v , j, 0.5 * (A[i + usize][j + usize] + A[j + usize][i + usize]));
              mat.accumulate(lfsu_v , i , lfsu_w , j, 0.5 * (A[i + usize][j + 2 * usize] + A[i + 2 * usize][j + usize]) );
              mat.accumulate(lfsu_w , i , lfsu_v , j, 0.5 * (A[i + usize][j + 2 * usize] + A[j + 2 * usize][i + usize]) );

              mat.accumulate(lfsu_w , i , lfsu_w , j, 0.5 * (A[i + 2*usize][j + 2 * usize] + A[i + 2*usize][j + 2 * usize]) );
            }
          }
        
        // Compute Darcy Term

        // evaluate gradient of basis functions on reference element
        std::vector<JacobianType> js_p(psize);
        lfsu_p.finiteElement().localBasis().evaluateJacobian(ip.position(),js_p);

        // transform gradients from reference element to real element
        
        std::vector<Dune::FieldVector<RF,dim> > gradpsi(lfsu_p.size());
        for (size_type i=0; i<lfsu_p.size(); i++)
          jac.mv(js[i][0],gradpsi[i]);
        
        Dune::FieldMatrix<double,dim,pdof> E(0.0);
          Dune::FieldMatrix<double,pdof,dim> ET(0.0);

          for (int i = 0; i < psize; i++){
            for (int j = 0; j < dim; j++){
              E[j][i] = gradpsi[i][j];
              ET[i][j] = E[j][i];
            }
          }

        Dune::FieldMatrix<double,pdof,pdof> K = ET.rightmultiplyany(E);

          K *= (dt_k * factor);

          for (int i = 0; i < psize; i++){
            for (int j = 0; j < psize; j++){
              mat.accumulate(lfsu_p , i , lfsu_p , j, 0.5 * (K[i][j] + K[j][i]));
            }
          }

        // evaluate basis functions on reference element
        std::vector<RangeType_P> psi(lfsu_p.size());
        lfsu_p.finiteElement().localBasis().evaluateFunction(ip.position(),psi);

        Dune::FieldMatrix<double,pdof,pdof> Mass(0.0);

        for (int i = 0; i < lfsu_p.size(); i++){
            for (int j = 0; j < lfsu_p.size(); j++){
                Mass[i][j] = psi[i] * psi[j];
            }
        }

        Mass *= (1.0/1000.0) * factor;

        for (int i = 0; i < psize; i++){
            for (int j = 0; j < psize; j++){
              mat.accumulate(lfsu_p , i , lfsu_p , j, 0.5 * (Mass[i][j] + Mass[j][i]));
            }
        }

        
        
    } // end for each quadrature point



}

    
    
// Volume Integral Depending on Test and Ansatz Functions
template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
{
    // Galerkin Finite Elements - assumes lfsu == lfsv
    const int dim = 3;

    // Unwrap shape local function spaces
    typedef typename LFSU::template Child<0>::Type LFSU_U;
    typedef typename LFSU::template Child<1>::Type LFSU_V;
    typedef typename LFSU::template Child<2>::Type LFSU_W;
    typedef typename LFSU::template Child<3>::Type LFSU_P;
              
    const LFSU_U& lfsu_u = lfsu.template child<0>();
    const LFSU_V& lfsu_v = lfsu.template child<1>();
    const LFSU_W& lfsu_w = lfsu.template child<2>();
    const LFSU_P& lfsu_p = lfsu.template child<3>();

    const unsigned int usize = lfsu_u.size();

    const unsigned int psize = lfsu_p.size();


    // domain and range field type
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainType D;
    typedef typename R::value_type RF;
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LFSU_U::Traits::SizeType size_type;
    typedef typename LFSU_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeType;
    typedef typename LFSU_P::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeType_P;
                
    // select quadrature rule
    auto geo = eg.geometry();
    
    const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(geo.type(),intorder);
                
    // Evaluate Cosserat tensor (assumes it is constant over a single element)
    Dune::FieldMatrix<double,6,6> C = param.ElasticTensor();

    RF dt_k = -dt * param.Permeability();
    
    
    // Unwrap solution at node, into vector d
    Dune::FieldVector<double,udof> d(0.0);
    Dune::FieldVector<double,pdof> p(0.0);
    for (size_type i=0; i < usize; ++i){
        d[i] = x(lfsu_u,i); // U
        d[i + usize] = x(lfsu_v,i); // V
        d[i + 2 * usize] = x(lfsu_w,i); // W
    }
    for (size_type i=0; i < psize; ++i){
        p[i] = x(lfsu_p,i);
    }

    int cnt = 0;
    
    // Loop over quadrature points
    for (const auto& ip : rule)
    {

        cnt += 1;
        
        // Evaluate shape functions at Integration Point
        std::vector<RangeType> phi(usize);    
        lfsu_u.finiteElement().localBasis().evaluateFunction(ip.position(),phi);
        
        // Evaluate gradients of shape function at integration point
        std::vector<JacobianType> js(usize);
        lfsu_u.finiteElement().localBasis().evaluateJacobian(ip.position(),js);
        
        // transform gradient to real element
        const typename EG::Geometry::JacobianInverseTransposed jac = geo.jacobianInverseTransposed(ip.position());
        
        std::vector<Dune::FieldVector<RF,dim> > gradphi(usize);
            for (size_t i=0; i<usize; i++){
                gradphi[i] = 0.0;
                jac.umv(js[i][0],gradphi[i]);
            }     

        // Compute B - such that Strain = B * d - Checked
        Dune::FieldMatrix<double,6,udof> B(0.0);
        for (size_type i = 0; i < usize; i++){
            B[0][i] = gradphi[i][0]; // E11
            B[1][i + usize] = gradphi[i][1]; // E22
            B[2][i + 2 * usize] = gradphi[i][2]; // E33
            B[3][i + usize] = gradphi[i][2]; B[3][i + 2 * usize] = gradphi[i][1]; // E23  = U2,3 + U3,2
            B[4][i] = gradphi[i][2]; B[4][i + 2 * usize] = gradphi[i][0]; // E13  = U1,3 + U3,1
            B[5][i] = gradphi[i][1]; B[5][i + usize] = gradphi[i][0]; // E12  = U1,2 + U2,1           
        }

        
        // Compute Residual of Cosserat Elastic Terms
        
        Dune::FieldVector<double,6> eps(0.0), sig(0.0);
        
        B.mv(d,eps); // eps = B * d
        
        C.mv(eps,sig); // sig = C * eps
        
        Dune::FieldVector<double,udof> res(0.0);
        
        B.mtv(sig,res); // res = Bt * sig;
        
        // Compute Darcy Term


        // evaluate gradient of basis functions on reference element
        std::vector<JacobianType> js_p(psize);
        lfsu_p.finiteElement().localBasis().evaluateJacobian(ip.position(),js_p);

        // transform gradients from reference element to real element
        
        std::vector<Dune::FieldVector<RF,dim> > gradpsi(lfsu_p.size());
        for (size_type i=0; i<lfsu_p.size(); i++)
          jac.mv(js[i][0],gradpsi[i]);
        
        // compute gradient of u
        Dune::FieldVector<RF,dim> gradp(0.0);
        for (size_type i=0; i<lfsu_p.size(); i++)
          gradp.axpy(x(lfsu_p,i),gradpsi[i]);

      // evaluate basis functions on reference element
        std::vector<RangeType_P> psi(lfsu_p.size());
        lfsu_p.finiteElement().localBasis().evaluateFunction(ip.position(),psi);

        Dune::FieldMatrix<double,pdof,pdof> Mass(0.0);

        for (int i = 0; i < lfsu_p.size(); i++){
            for (int j = 0; j < lfsu_p.size(); j++){
                Mass[i][j] = psi[i] * psi[j];
            }
        }

        Mass *= (1.0 / 1000.00);


        Dune::FieldVector<double,pdof> my_res(0.0);

        Mass.mv(p,my_res);
        


        // geometric weight
        RF factor = geo.integrationElement(ip.position()) * ip.weight();
    
        
        for (size_type i=0; i < usize; i++){
            r.accumulate(lfsu_u,i,(res[i])  * factor);
            r.accumulate(lfsu_v,i,(res[i + usize]) * factor);
            r.accumulate(lfsu_w,i,(res[i + 2*usize]) * factor);
        }

         for (size_type i=0; i<lfsu_p.size(); i++){
          r.accumulate(lfsu_p,i,(dt_k *(gradp*gradpsi[i]))*factor);
        }
        
    } // end for each quadrature point
                
} // end alpha_volume


 // residual for boundary term
    template<typename IG, typename LFSV, typename R>
    void lambda_boundary (const IG& ig, const LFSV& lfsv, R& r) const
    {
        
        const int dim = 3;
        const int dimw = 3;
        
        // extract local function spaces
        typedef typename LFSV::template Child<0>::Type LFSV_U;
        typedef typename LFSV::template Child<1>::Type LFSV_V;
        typedef typename LFSV::template Child<2>::Type LFSV_W;
        
        const LFSV_U& lfsv_u = lfsv.template child<0>();
        const LFSV_V& lfsv_v = lfsv.template child<1>();
        const LFSV_W& lfsv_w = lfsv.template child<2>();
               
        const unsigned int usize = lfsv_u.size();
        
        // domain and range field type
        typedef typename LFSV_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename R::value_type RF;
        typedef typename LFSV_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename LFSV_U::Traits::SizeType size_type;
        typedef typename LFSV_U::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RT_V;
        
        // select quadrature rule
        auto geo = ig.geometry();
        const Dune::QuadratureRule<DF,dim-1>& rule = Dune::QuadratureRules<DF,dim-1>::rule(geo.type(),intorder);
        
        int counter = 0;

        int model_type  = param.getModelType();

        if (model_type == 2){
            for (int i = 0; i < geo.corners(); ++i){
                if(std::abs(geo.corner(i).two_norm() - 1.0) < 1e-6){ counter += 1; }
            }
        }
        else{ //model type 1

            double Lz = param.getL(2);

            for (int i = 0; i < geo.corners(); ++i){
                if(std::abs(geo.corner(i)[2] - Lz) < 1e-6){ counter += 1; }
            }


        }


        if (counter == geo.corners()){


        // loop over quadrature points and integrate normal flux
        for (const auto& ip : rule)
        {
            // position of quadrature point in local coordinates of element
            Dune::FieldVector<DF,dim> local = ig.geometryInInside().global(ip.position());
            
            // evaluate basis functions
            std::vector<RT_V> phi(usize);
            lfsv_u.finiteElement().localBasis().evaluateFunction(local,phi);
            
            // Compute Weight Factor
            const RF factor = ip.weight() * geo.integrationElement(ip.position());
            
            const Dune::FieldVector<DF,dimw> normal = ig.unitOuterNormal(ip.position());

            const Dune::FieldVector<DF,dimw> x = geo.global(ip.position());

            double ap = param.getAppliedPressure();
            
            for (size_t i=0; i < usize; i++){
                r.accumulate(lfsv_u,i, -ap * normal[0] * phi[i] * factor);
                r.accumulate(lfsv_v,i, -ap * normal[1] * phi[i] * factor);
                r.accumulate(lfsv_w,i, -ap * normal[2] * phi[i] * factor);
            }

            
        }
    }
    
        
        
    } // end boundary term


    
    
        private:
    
            const PARAM& param;
            int intorder;
            double dt;
};
        
