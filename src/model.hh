template<int dim>
class MODEL{

public:

	MODEL(Dune::ParameterTree& configuration){

		Model_Type = configuration.get<int>("Model_Type",1);

		bM = configuration.get<double>("Bulk_Modulus",1000.0);
		nu = configuration.get<double>("Poisson_Ratio",0.25);
		b = configuration.get<double>("Biot_Coefficient",1.0);
		m = configuration.get<double>("Biot_Modulus",10e6);

		permeability = configuration.get<double>("Permeability",10e-12);
		mu = configuration.get<double>("Visocity",10e-6);

		applied_pressure = configuration.get<double>("applied_pressure",-1.0);
		radius = configuration.get<double>("Radius",1.0);

		timeStep = configuration.get<double>("time_step",1.0);

		TotalTime = configuration.get<double>("total_time",556.0);

		SolverTolerance = configuration.get<double>("solver_tolerance",1e-10);
		NewtonTolerance = configuration.get<double>("newton_tolerance",1e-10);

		rho = phi * rho_f + (1.0 - phi) * rho_s;

		L[0] = configuration.get<double>("Lx", 1.0);
		L[1] = configuration.get<double>("Ly", 1.0);
		L[2] = configuration.get<double>("Lz",1.0);

		N[0] = configuration.get<int>("Nx",5);
		N[1] = configuration.get<int>("Ny",5);
		N[2] = configuration.get<int>("Nz",5);

		periodic[0] = configuration.get<bool>("Periodic_x",false);
		periodic[1] = configuration.get<bool>("Periodic_y",false);
		periodic[2] = configuration.get<bool>("Periodic_z",false);

		overlap = configuration.get<int>("overlap",3);

	}

	double inline getT(){return TotalTime;}
	double inline get_dt(){ return timeStep; }
	double inline getReduction(){return SolverTolerance; }

	int inline getModelType() const{	return Model_Type;	}

	bool inline isDirichlet(const Dune::FieldVector<double,3>& x, int dof) const{
		// isDirichlet returns bool if degree of freedom (dof) at node with coords (x) lies on boundary
		bool Dirich = false; // Initialise to default value
		
		if(Model_Type == 2){

		if (dof < 4){
			if ((x[0] < 1e-6) && dof == 1){ Dirich = true; }
			if ((x[1] < 1e-6) && dof == 2){ Dirich = true; }
			if ((x[2] < 1e-6) && dof == 3){ Dirich = true; }
		}


		if ((dof == 4) && (std::abs(x.two_norm() - radius) < 1e-2)) {	Dirich = true;	}
		}
		else{ 

			if ((x[2] < 1e-6) && dof < 4){ Dirich = true; }

			if (dof == 4){
				if(x[0]<1e-6){ Dirich = true;}
				if(x[1]<1e-6){ Dirich = true;}
				if(x[2]>L[2]-1e-6){ Dirich = true;}
				if(x[0]>L[1]-1e-6){ Dirich = true;}
				if(x[1]>L[0]-1e-6){ Dirich = true;}
			}

		}
		return Dirich; // return bool
	} // end isDirichlet

	double inline evaluateDirichlet(const Dune::FieldVector<double,3>& x, int dof) const{
		// Returns the value at a boundary node (at x) for degree of freedom dof
		double value = 0.0;
		return value;
	}

	bool inline isNeumann(const Dune::FieldVector<double,3>& x, int dof){
		
		bool Neumann = false;
		if (x[2] > L[2] - 1e-6){ Neumann = true;}

		return Neumann;
	}

	double inline getAppliedPressure() const{
		return applied_pressure;
	}

	Dune::FieldMatrix<double,6,6> inline ElasticTensor() const{
		Dune::FieldMatrix<double,6,6> C(0.0);
		double lambda = 3.0 * bM * nu / (1.0 + nu);
		double G = 3.0 * bM * (1.0 - 2.0 * nu) / (2.0 * (1.0 + nu));
		for (int i = 0; i < 3; i++){ C[i][i] = 2.0 * G; }
		for (int i = 0; i < 3; i++){	for (int j = 0; j < 3; j++){	C[i][j] += lambda;	}	}
		for (int i = 3; i < 6; i++){ C[i][i] = G; }
		return C;
	}

	double inline Permeability() const{
		double K = permeability / mu;
		return K;
	}

	Dune::FieldVector<double,dim> inline getL(){return L;}
	double inline getL(int i) const{return L[i];}
	std::bitset<dim> inline getPeriodic(){return periodic;}
	Dune::array<int,dim> inline getN(){return N;}
	int inline getOverlap(){return overlap;}
	double inline get_drained_Mod(){return bM;}

	// Function for geometry transformation of rectangular grid to part geometry (to be defined by the user)

	Dune::FieldVector<double,dim> inline evaluateGeometryTransformation(const Dune::FieldVector<double,dim>& x){

		Dune::FieldVector<double,dim> y(0.0), final(0.0);

		if(Model_Type == 2){
		
		for(int i = 0; i < 3; i++){ y[i] = x[i] ; }

		final[0] = y[0] * std::sqrt( 1.0 - y[1] * y[1]/2.0 - y[2] * y[2]/2.0 + y[1] * y[1] * y[2] * y[2] / 3.0 );
		final[1] = y[1] * std::sqrt( 1.0 - y[2] * y[2]/2.0 - y[0] * y[0]/2.0 + y[0] * y[0] * y[2] * y[2] / 3.0 );
		final[2] = y[2] * std::sqrt( 1.0 - y[1] * y[1]/2.0 - y[0] * y[0]/2.0 + y[1] * y[1] * y[0] * y[0] / 3.0 );

		}
		else{

			final = x;
		}

		return final;
	}

private:
	double bM, nu; // Elastic Material Properties
	double permeability, mu; // Permeability Material Properties
	double b; // Biot's Coefficient
	double m; // Biot's Modulus
	double phi; // porosity
	double rho_s, rho_f; // densities of solid and fluid respectively
	double rho;
	double applied_pressure;
	double timeStep, TotalTime;
	double SolverTolerance;
	double NewtonTolerance;
	double radius; // Radius of Sphere

	int Model_Type;


	int overlap;
	Dune::FieldVector<double,dim> L;
	std::bitset<dim> periodic;
	Dune::array<int,dim> N;



};
